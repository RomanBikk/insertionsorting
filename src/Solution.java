import java.util.Arrays;

public class Solution {
    public static void main(String[] args) {
        int[] test = new int[10];
        fillArray(test);
        for (int i : test) {
            System.out.print(i + " ");

        }
        System.out.println();
        insertionSort(test);


    }

    private static void fillArray(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * 10);

        }
    }

    private static void insertionSort(int[] a) {
        for (int i = 0; i < a.length; i++) {
            int value = a[i];
            int j = i - 1;
            for (; j >= 0; j--) {
                if (value < a[j]) {
                    a[j + 1] = a[j];
                } else break;
            }
            a[j + 1] = value;

        }
        for (int i : a) {
            System.out.print(i + " ");

        }

    }

}
